There are two javascript files:

* index.js - The express site
* basket.js - The basket, holding the state

To run the tests:

`npm install`
`./node_modules/jasmine/bin/jasmine.js test/basketSpec.js`

To run the app:

`node app/index.js`


There are two endpoints:

1) Post a new item:

`curl -XPOST http://localhost:3000/basket/bread`

2) Total

`curl -XGET http://localhost:3000/total`

The total will respond with a JSON including: subTotal, total and discounts