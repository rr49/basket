let Basket    = require('../app/basket.js');
let priceList = require('./testPrices.json');


describe('Basket', () =>{
    it('should have no items when it is created', ()=>{
        let b = Basket();
        let items = b.items();
        expect(items).toEqual([]);
    });

    it('should be able to add one item', ()=>{
        let b = Basket();
        b.addItem('apple');
        let items = b.items();
        expect(items).toEqual(['apple']);
    });


    describe('groupItems', () =>{
        it('should return empty when there are no items', ()=>{
            let b = Basket();
            let group = b.groupItems([]);
            expect(group).toEqual({});
        });

        it('should group items by name together with the occurences', ()=>{
            let b = Basket();
            let group = b.groupItems(['apple', 'apple', 'milk']);
            expect(group).toEqual({apple:2, milk:1});
        })
    })

    describe('priceItems', () => {
        it('should return empty if there are no items', () => {
            let b = Basket();
            var priced = b.priceItems(priceList, []);
            expect(priced).toEqual({});
        });

        it('should return grouped items with price', () => {
            let b = Basket();
            var priced = b.priceItems(priceList, ['apples', 'apples', 'milk', 'milk']);
            expect(priced).toEqual({
                apples: {
                    quantity: 2,
                    price: 2
                },
                milk: {
                    quantity: 2,
                    price: 2.6
                }
            })
        })
    });

    describe('discountApples', () => {
        it('should apply the discount for apples', () =>{
            let expected = {
                    quantity: 2,
                    price: 2.6,
                    discount: 0.26
            }
            let b = Basket();
            let discounted = b.discountApples({quantity: 2, price:2.6});
            expect(discounted).toEqual(expected);
        });

    });

    describe('discountBread', () => {

        it('should return the bread with no discount  if there are no tins of soup', () => {
            let b = Basket();
            let bread = {quantity:1, price:1}
            let discounted = b.discountBread(bread, {});
            expect(discounted).toEqual({quantity:1, price:1});
        });


        it('should return the same input if there are less than two soups', () =>{
            let bread = {quantity:1, price:1}
            let b = Basket();
            let discounted = b.discountBread(bread, {soup: {quantity:1, price: 1}});
            expect(discounted).toEqual(bread);
        });

        it('should apply half price discount if there are two soups', () => {
            let bread = {quantity: 1, price: 0.8 };
            let expected = {
                    quantity: 1,
                    price: 0.8,
                    discount: 0.4
            }
            let b = Basket();
            let discounted = b.discountBread(bread, {soup: {quantity:2, price: 1}});
            expect(discounted).toEqual(expected);
        });


        it('should apply half price discount if there are two soups and more breads', () => {
            let bread = {quantity: 2, price: 1.6 };
            let expected = {
                    quantity: 2,
                    price: 1.6,
                    discount: 0.4
            }
            let b = Basket();
            let discounted = b.discountBread(bread, {soup: {quantity:2, price: 1}});
            expect(discounted).toEqual(expected);
        });


        it('should discount entirely if there are four soups', () => {
            let bread = {quantity: 1, price: 0.8 };
            let expected = {
                    quantity: 1,
                    price: 0.8,
                    discount: 0.8
            }
            let b = Basket();
            let discounted = b.discountBread(bread, {soup: {quantity:4, price: 1}});
            expect(discounted).toEqual(expected);
        });


        it('should discount the maximum of the bread price when there are too many soups', () => {
            let bread = {quantity: 1, price: 0.8 };
            let expected = {
                    quantity: 1,
                    price: 0.8,
                    discount: 0.8
            }
            let b = Basket();
            let discounted = b.discountBread(bread, {soup: {quantity:6, price: 0.8}});
            expect(discounted).toEqual(expected);
        })
    });


    describe('applyDiscounts', () => {

        it('should return an empty object if there are no items', () => {
            let itemList = [];
            let expected = {};
            let prices = {};
            let discounts = {};
            let b = Basket();
            let result = b.applyDiscounts(prices, discounts, itemList)
            expect(result).toEqual({});
        });


        it('should return an object of priced items if there are no discounts', () => {
            let itemList = ['milk'];
            let expected = {
                milk: {
                    quantity: 1,
                    price: 1
                }
            }
            let prices = {milk: {price: 1}};
            let discounts = {};
            let b = Basket();
            let result = b.applyDiscounts(prices, discounts, itemList)
            expect(result).toEqual({milk: {quantity:1, price: 1}})
        });

        it('should return an object of priced items with discounts if there are discounts', () => {
            let itemList = ['apples'];
            let expected = {
                apples: {
                    quantity: 1,
                    price: 1.0
                }
            }
            let prices = {apples: {price: 1}};

            let b = Basket();
            let discounts = {apples: b.discountApples};
            let result = b.applyDiscounts(prices, discounts, itemList)
            expect(result).toEqual({apples: {quantity:1, price: 1, discount: 0.1}})
        })
    })



    describe('displayTotal', () => {
        it('should return an empty object if there are no items', () => {
            let input = {};
            let b = Basket();
            expect(b.displayTotal(input)).toEqual({});
        });

        it('should return the total without discounts if there are no discounts', () => {
            let input = {
                apples: { quantity:1, price: 1.0},
                soups: { quantity: 3, price: 4.0},
                bread: { quantity:1, price: 5.0}
            };
            let expected = {
                subTotal: 10.0,
                total: 10.0,
                discount: [{
                    label: "No discount",
                    amount: 0
                }]
            }
            let b = Basket();
            expect(b.displayTotal(input)).toEqual(expected);
        });


        it('should return the total with discounts if there are some discounts', () => {
            let input = {
                apples: { quantity: 1, price: 1, discount: 0.1},
                soups: { quantity: 2, price: 0.65},
                bread: { quantity: 1, price: 0.8, discount: 0.4}
            };
            let expected = {
                subTotal: 2.45,
                total: 2.45 - 0.5,
                discount: [
                    { label: 'Apples 10% off:', amount: 0.1 },
                    { label: 'Two tins of soup 50% off', amount: 0.4 }
                ]
            }

            let b = Basket();
            expect(b.displayTotal(input)).toEqual(expected);
        });



    });




























































});

