const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const Basket     = require('./basket');
const prices     = require('./prices.json');

let basket = Basket();

app.use(bodyParser.json());

app.listen(3000, () => console.log('Example app listening on port 3000!'));

app.post('/basket/:item', (req, res) => {
    let item = req.params.item;
    if (prices[item]) {
        console.log("Adding item", item);
        basket.addItem(item);
        res.status(200).send("ok");
    } else {
        res.sendStatus(404);
    }
});


app.get('/total', (req, res) => {
    let items  = basket.items();
    let discounted = basket.applyDiscounts(prices, basket.discounts, items);
    let total  = basket.displayTotal(discounted);
    res.status(200).send(total);
});


