"use strict";
let _ = require('lodash');

function Basket(){
    // The state of a basket is stored in _items, a private variable under closure
    let _items = [];

    let addItem = (item) => _items.push(item);
    let items   = () => _items;

    let groupItems = (items) => _.mapValues(_.groupBy(items), (values,key) => values.length);

    function priceItems(prices, items) {
        let priceItem   = (prices, item) => prices[item] ? prices[item].price : null;
        return _.mapValues(groupItems(items), (values, k) => {
            return {
                quantity: values,
                price: priceItem(prices,k) * values
            };
        });
    };

    function discountApples(item) {
        return {
            quantity: item.quantity,
            price: item.price,
            discount: item.price * 0.1
        };
    }

    function discountBread(item, groupedItems){
        let bread = {
            quantity: item.quantity,
            price: item.price
        };
        if (_.get(groupedItems, 'soup.quantity') >= 2){
            let tins = groupedItems.soup.quantity;
            bread.discount = Math.min(bread.price, bread.price/bread.quantity * 0.5 * Math.floor(tins/2));
        }
        return bread;
    }

    let discounts = {
        apples: discountApples,
        bread: discountBread
    };

    function applyDiscounts(prices, discounts, itemList) {
        let priced = priceItems(prices, itemList);
        let discounted = _.mapValues(priced, (v, k) => (discounts[k]) ? discounts[k](v, priced) : v);
        return discounted;
    }

    function displayTotal(discountedItems) {

        let discountsLabels = {
            apples: 'Apples 10% off:',
            bread:  'Two tins of soup 50% off'
        };

        let subTotal = _.compact(_.map(discountedItems, (value, key) => value.price));
        let sum = _.reduce(subTotal, (a,b) => a + b);

        let labels = _.compact(_.map( discountedItems, (v, k) =>
            (discountsLabels[k] && v.discount)
            ? { label: discountsLabels[k], amount: v.discount }
            : null
        ));

        let discountTotal = (
            (_.isArray(labels) && !_.isEmpty(labels))
            ? _.reduce(_.map(labels, 'amount'), (a,b) => a + b)
            : 0
        );

        let discountMessage = (discountTotal) ? labels : [{ label: "No discount",  amount: 0 }];

        return ((sum && sum > 0 )
                ? { subTotal: sum, total: sum - discountTotal, discount: discountMessage}
                : { });

    }

    return {
        discounts,
        addItem,
        items,
        groupItems,
        discountApples,
        priceItems,
        discountBread,
        applyDiscounts,
        displayTotal
    };
}

module.exports  = Basket;